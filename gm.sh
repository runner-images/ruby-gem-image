wget --no-cookies --timestamping https://downloads.sourceforge.net/project/graphicsmagick/graphicsmagick/1.3.26/GraphicsMagick-1.3.26.tar.gz --directory /tmp

if [ ! -d "/opt/graphicsmagick/src" ]; then
  mkdir --parents /opt/graphicsmagick/src
fi
tar zxvf /tmp/GraphicsMagick-1.3.26.tar.gz --directory /opt/graphicsmagick/src

# Build binaries
cd /opt/graphicsmagick/src/GraphicsMagick-1.3.26
./configure --prefix=/opt/graphicsmagick/GraphicsMagick-1.3.26
make
make install

# Update path references in system profile and make it default
update-alternatives --install "/usr/bin/gm" "gm" "/opt/graphicsmagick/GraphicsMagick-1.3.26/bin/gm" 1
update-alternatives --set gm /opt/graphicsmagick/GraphicsMagick-1.3.26/bin/gm