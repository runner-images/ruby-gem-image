FROM ruby:latest

# Update apt-get
RUN apt-get update && apt-get install -y apt-utils && apt-get install -y curl


# Install build essentials, for compiling and stuff
RUN apt-get install -y build-essential 
RUN apt-get install -y libgdiplus
RUN apt-get install -y software-properties-common
RUN apt-get install -y libjpeg-dev
RUN apt-get install -y libtiff5-dev
RUN apt-get install -y libpng-dev

# Install PngQuant
RUN git clone --recursive https://github.com/pornel/pngquant.git
WORKDIR pngquant
RUN make
RUN make install
WORKDIR ..	

# Install JpegOptim
RUN apt-get install -y jpegoptim

# Graphics Magic
COPY . .
RUN chmod u+x gm.sh
RUN ./gm.sh


